#!/bin/bash

declare -a indices

export indices=(
  camoproxy
  chef
  consul
  fluentd
  gitaly
  gke
  gke-audit
  gke-systemd
  jaeger
  kas
  mailroom
  monitoring
  pages
  postgres
  praefect
  pubsubbeat
  puma
  rails
  redis
  registry
  runner
  shell
  sidekiq
  system
  vault
  workhorse
)
